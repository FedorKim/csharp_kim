﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Levenshtein
{
    class Program
    {
        static int Levenshtein(string a, string b)
        {
            int[,] d = new int[a.Length + 1, b.Length + 1];
            for (int i = 0; i <= a.Length; i++)
            {
                d[i, 0] = i;
            }
            for (int j = 0; j <= b.Length; j++)
            {
                d[0, j] = j;
            }

            for (int i = 1; i <= a.Length; i++)
            {
                for (int j = 1; j <= b.Length; j++)
                {
                    int mn1 = (d[i - 1, j] + 1 < d[i, j - 1] ? d[i - 1, j] + 1 : d[i, j - 1] + 1);
                    int k = (a[i - 1] != b[j - 1] ? 2 : 0);
                    int mn2 = (d[i - 1, j - 1] + k < mn1 ? d[i - 1, j - 1] + k : mn1);
                    d[i, j] = mn2;
                }
            }
            return d[a.Length, b.Length];
        }
        static void Main(string[] args)
        {
            StreamReader Dictionary = new StreamReader(@"C:\Levenshtein\words.txt");
            StreamReader Input = new StreamReader(@"C:\Levenshtein\input.txt");
            StreamWriter Output = new StreamWriter(@"C:\Levenshtein\output.txt");
            string[] ar = Input.ReadToEnd().Split(' ');
            var words = new List<string>();
            string line;
            while ((line = Dictionary.ReadLine()) != null)
            {
                words.Add(line);
            }
            for (int i = 0; i < ar.Length; i++)
            {
                string s = ar[i].ToLower();
                bool iscorrect = false;
                int mndist = -1;
                string tmp = " ";
                for (int j = 0; j < words.Count; j++)
                {
                    string t = words[j];
                    int dist = Levenshtein(s, t);
                    if (dist == 0) iscorrect = true;
                    else if (mndist == -1 || mndist > dist)
                    {
                        mndist = dist;
                        tmp = t;
                    }
                }
                if (iscorrect == true)
                {
                    Output.Write(ar[i] + " ");
                    Console.WriteLine(ar[i] + " - Right word");
                }
                else
                {
                    Output.Write(tmp + " ");
                    Console.WriteLine(ar[i] + " - " + tmp);
                }
            }
            Console.ReadKey();
            Dictionary.Close();
            Input.Close();
            Output.Close();
        }
    }
}
