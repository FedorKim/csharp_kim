﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inheritance11
{
    public abstract class Vehicle
    {
        abstract public string properties();
    }
    class Car : Vehicle
    {
        double price, speed;
        int date;
        public Car(double _price, double _speed, int _date)
        {
            price = _price;
            speed = _speed;
            date = _date;
        }
        public override string properties()
        {
            string s = price + " " + speed + " " + date + " ";
            return s;
        }

    }
    class Plane : Vehicle
    {
        double height;
        int passenger;
        public Plane( double _height, int _passenger)
        {
            passenger = _passenger;
            height = _height;

        }
        public override string properties()
        {
            string s = passenger + " " + height + " ";
            return s;
        }
    }
    class Ship : Vehicle
    {
        int passenger, date;
        string Port;
        public Ship(int _passenger, string _Port)
        {
            passenger = _passenger;
            Port = _Port;
        }
        public override string properties()
        {
            string s = passenger + " " + Port + " ";
            return s;
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Car MyCar = new Car(30000, 300, 2012);
            Plane MyPlane = new Plane(4, 350);
            Ship MyShip = new Ship(1300, "st.Jones");
            Console.WriteLine(MyCar.properties());
            Console.WriteLine(MyShip.properties());
            Console.WriteLine(MyPlane.properties());
            Console.ReadKey();
        }
    }
}