﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abstract
{
    abstract class Figure
    {
        public double Area;
        public double Perimeter;
        public string type;
    }
    class Rectangle : Figure
    {
        private int a; 
        private int b;
        public Rectangle(int _a, int _b)
        {
            a = _a;
            b = _b;
            this._perimeter();
            this._area();
        }
        private void _area()
        {
            double A;
            A = a * b;
            this.Area = A;
        }
        private void _perimeter()
        {
            double P;
            P = (a + b) * 2;
            this.Perimeter = P;
        }
    }
    class Circle : Figure
    {
        double R;
        public Circle(double _R)
        {
            R = _R;
            this._area();
            this._perimeter();
        }
        private void _area()
        {
            double A1;
            A1 = 3.14 * R * R;
            this.Area = A1;
        }
        private void _perimeter()
        {
            double P1;
            P1 = 2 * 3.14 * R;
            this.Perimeter = P1;
        }
    }
    class Triangle : Figure
    {
        private int a;
        private int b;
        private int c;
        public Triangle(int _a, int _b, int _c)
        {
            a = _a;
            b = _b;
            c = _c;
            this._perimeter();
            this._area();
        }
        private void _area()
        {
            double p;
            double h;
            double A2;
            p = (0.5) * (a + b + c);
            h = (2 * Math.Sqrt(p * (p - a) * (p - b) * (p - c))) / a;
            A2 = 0.5 * h * a;
            this.Area = A2;

        }
        private void _perimeter()
        {
            double P2;
            P2 = a + b + c;
            this.Perimeter = P2;
        }
     
    }
    class Program
    {
        
        static void Main(string[] args)
        {
            Rectangle rec = new Rectangle(10, 10);
            Circle cir = new Circle(5);
            Triangle tri = new Triangle(5, 5, 5);
            Console.WriteLine(rec.Area + " " + rec.Perimeter);
            Console.WriteLine(cir.Area + " " + cir.Perimeter);
            Console.WriteLine(tri.Area + " " + tri.Perimeter);
            Console.ReadKey();
        }
    }
}
