﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Xml.Serialization;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Serialization1
{
    public enum Genders : int { male, female };
    public class Student
    {
        public string name;
        public string surname;
        [XmlIgnore]
        public Genders gender;
        public double gpa;
        public Student()
        {

        }
        public Student(string _name, string _surname,  Genders _gender, double _gpa)
        {
            name = _name;
            surname = _surname;
            gender = _gender;
            gpa = _gpa;
        }
        public override string ToString()
        {
            return name + " " + surname + " " + " " + gender + " " + gpa;
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Student a = new Student("Fedor", "Kim", Genders.male, 3.1);
            XmlSerializer ser = new XmlSerializer(typeof(Student));
            StreamWriter sw = new StreamWriter(@"C:\Users\Fora1\Desktop\Alltasks\students.xml");
            ser.Serialize(sw, a);
            sw.Close();
            XmlSerializer b = new XmlSerializer(typeof(Student));
            StreamReader sr = new StreamReader(@"C:\Users\Fora1\Desktop\Alltasks\students.xml");
            Student s = new Student();
            s = (Student)b.Deserialize(sr);
            sr.Close();
            Console.WriteLine("ready!");
            Console.ReadKey();
        }
    }
}
