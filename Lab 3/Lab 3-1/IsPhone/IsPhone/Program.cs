﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace IsPhone
{
    class Program
    {
        static bool IsZip(string s)
        {
            return Regex.IsMatch(s, @"^\d{5}(\-\d{4})?$");
        }
                static bool IsPhone(string s)
        {
            return Regex.IsMatch(s, @"^\(?\d{3}\)?[\s\-]?\d{3}\-?\d{4}$");
        }

        static void Main(string[] args)
        {
            Console.WriteLine();
            string input = Console.ReadLine();
            if (IsPhone(input) == true)
            {
                Console.WriteLine("is a Phone Number");
            }
            else if (IsZip(input) == true)
            {
                Console.WriteLine("is a Zip Code");
            }
            else
            {
                Console.WriteLine("is unknown");
            }
            Console.ReadKey();
        }
    }
}
