﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.IO.Compression;


namespace CompressionDemo_1_
{
    class Program
    {
        static void UncompressFile(string inFilename, string outFilename)
        {
            FileStream sourceFile = File.Open(inFilename, FileMode.Open);
            FileStream destFile = new FileStream(outFilename, FileMode.Create);
            GZipStream compStream = new GZipStream(sourceFile, CompressionMode.Decompress);
            int theByte = compStream.ReadByte();
            while (theByte != -1)
            {
                destFile.WriteByte((byte)theByte);
                theByte = compStream.ReadByte();
            }
        }
        static void Main(string[] args)
        {
            UncompressFile(@"c:\Papka\add.txt.gz", @"c:\File\add.txt.test");
        }
    }
}
