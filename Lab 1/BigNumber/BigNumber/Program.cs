﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigNumber
{
    
    struct BigNumber
    {
        public int[] a;
        public int n;

        public BigNumber(string s)
        {
            a = new int[1000];
            n = s.Length;
            for (int i = s.Length - 1, j = 0; i >= 0; i--, j++)
            {
                a[j] = s[i] - '0';
            }
        }
        


        public override string ToString()
        {
            string s = "";
            for (int i = n - 1; i >= 0; i--)
            {
                s = s + a[i].ToString();
            }
            return s;
        }


        public static BigNumber operator +(BigNumber a, BigNumber b)
        {
            BigNumber A = new BigNumber("0");
            int M = Math.Max(a.n, b.n);
            int k = 0;
            A.n = M;
            for (int i = 0; i < M; i++)
            {
                A.a[i] = (a.a[i] + b.a[i] + k);
                k = A.a[i] / 10;
                A.a[i] %= 10;
            }
            if (k > 0)
            {
                A.n += 1;
                A.a[A.n - 1] = k;
            }
            return A;
        }

    }
    

    class Program
    {
        static void Main(string[] args)
        {
            BigNumber a = new BigNumber(Console.ReadLine());
            BigNumber b = new BigNumber(Console.ReadLine());
            Console.WriteLine(a + b);
            Console.ReadKey();
        }
    }
}
