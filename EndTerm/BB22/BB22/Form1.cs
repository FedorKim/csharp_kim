﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BB22
{
    public partial class Form1 : Form
    {
        MyButton[,] bt = new MyButton[10, 10];
        public Form1()
        {
            InitializeComponent();
            for (int i = 0; i < 10; i++)
            {
                for (int j = 0; j < 10; j++)
                {
                    bt[i, j] = new MyButton(i, j);
                    bt[i, j].Click += new EventHandler(btn_click);
                    bt[i, j].BackColor = Color.White;
                    bt[i, j].Size = new Size(30, 30);
                    bt[i, j].Location = new Point(i * 30, j * 30);
                    this.Controls.Add(bt[i, j]);
                }
            }
        }

        private void btn_click(object sender, EventArgs e)
        {
            MyButton b = sender as MyButton;
              bt[b.x, b.y].BackColor = Color.White;

              if (bt[b.x-1, b.y].BackColor == Color.Black)
              {
                  bt[b.x - 1, b.y].BackColor = Color.White;
              }
              else bt[b.x-1, b.y].BackColor = Color.Black;

              if (bt[b.x+1, b.y].BackColor == Color.Black)
              {
                  bt[b.x + 1, b.y].BackColor = Color.White;
              }
              else bt[b.x+1, b.y].BackColor = Color.Black;
             
            if (bt[b.x, b.y-1].BackColor == Color.Black)

              {
                  bt[b.x, b.y - 1].BackColor = Color.White;
              }
              else bt[b.x, b.y-1].BackColor = Color.Black;


              if (bt[b.x, b.y+1].BackColor == Color.Black)
              {
                  bt[b.x, b.y + 1].BackColor = Color.White;
              }
              else bt[b.x, b.y+1].BackColor = Color.Black;

             if (bt[b.x, b.y].BackColor == Color.Black)
            {
                bt[b.x, b.y].BackColor = Color.White;
            }
            else bt[b.x, b.y].BackColor = Color.Black;
        }
        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}
