﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TimerFlow
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        public int s = 5;


        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            Graphics g = this.CreateGraphics();
            Pen p = new Pen(Color.Red, 10);
            if (s % 3 == 0)
            {
                g.DrawLine(p, 150, 0, 150, 60);
                g.DrawLine(p, 150, 70, 150, 130);
                g.DrawLine(p, 150, 140, 150, 200);
                g.DrawLine(p, 150, 210, 150, 270);
                g.DrawLine(p, 150, 280, 150, 340);
                g.DrawLine(p, 150, 350, 150, 410);
            }
            if (s % 3 == 1)
            {
                g.DrawLine(p, 150, 10, 150, 70);
                g.DrawLine(p, 150, 80, 150, 140);
                g.DrawLine(p, 150, 150, 150, 210);
                g.DrawLine(p, 150, 220, 150, 280);
                g.DrawLine(p, 150, 290, 150, 350);
                g.DrawLine(p, 150, 360, 150, 420);
            }
            if (s % 3 == 2)
            {
                g.DrawLine(p, 150, 20, 150, 80);
                g.DrawLine(p, 150, 90, 150, 150);
                g.DrawLine(p, 150, 160, 150, 220);
                g.DrawLine(p, 150, 230, 150, 290);
                g.DrawLine(p, 150, 300, 150, 360);
                g.DrawLine(p, 150, 370, 150, 430);
            }
            s++;
        }

        private void timer2_Tick(object sender, EventArgs e)
        {

        }
    }
}
